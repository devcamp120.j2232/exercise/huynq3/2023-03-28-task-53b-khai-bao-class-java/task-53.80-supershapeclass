import com.dev.Shape;
import com.dev.Circle;
import com.dev.Rectangle;
import com.dev.Square;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Shape shape1 = new Shape();
        Shape shape2 = new Shape("green", false);

        System.out.println(shape1);
        System.out.println(shape2);
        System.out.println("---sub 7 khai báo 3 Circle---phía dưới----");
        //sub 7 khai báo 3 Circle
        Circle circle1= new Circle();
        Circle circle2= new Circle(2.0);
        Circle circle3= new Circle("green", true ,3.0);
        System.out.println(circle1);
        System.out.println(circle2);
        System.out.println(circle3);
        System.out.println("---sub 8  khai báo 3 Rectangle---phía dưới----");
        //khai bao 3 rectangle
       
        Rectangle rectangle1= new Rectangle();
        Rectangle rectangle2= new Rectangle(2.5, 1.5);
        Rectangle rectangle3= new Rectangle("green",true, 3.0, 1.5);
        System.out.println(rectangle1);
        System.out.println(rectangle2);
        System.out.println(rectangle3);
        System.out.println("---sub 9  khai báo 3 Square---phía dưới----");
        //khai 3 Square

        Square square1= new Square ();
        Square square2= new Square (1.5);
        Square square3= new Square ("green", true, 2.0);
        System.out.println(square1);
        System.out.println(square2);
        System.out.println(square3);
        //sub 10 in ra dien tich  và chu vi 3 cái Circle
        System.out.println("Diện tích cirlce1: "+circle1.getArea()+"; &&& Chu vi: "+circle1.getPerimeter());
        System.out.println("Diện tích cirlce2: "+circle2.getArea()+"; &&& Chu vi: "+circle2.getPerimeter());
        System.out.println("Diện tích cirlce3: "+circle3.getArea()+"; &&& Chu vi: "+circle3.getPerimeter());
        // sub 11 : in ra chu vi va dien tich 3 cái Rectangle
        System.out.println("rectangle1: Diện tích : "+rectangle1.getArea()+"; &&& Chu vi: "+rectangle1.getPerimeter());
        System.out.println("rectangle2: Diện tích : "+rectangle2.getArea()+"; &&& Chu vi: "+rectangle2.getPerimeter());
        System.out.println("rectangle3: Diện tích : "+rectangle3.getArea()+"; &&& Chu vi: "+rectangle3.getPerimeter());
        // sub 12: in dien tich- chu vi 3 cái square
        System.out.println("square1: Diện tích : "+square1.getArea()+"; &&& Chu vi: "+square1.getPerimeter());
        System.out.println("square2: Diện tích : "+square2.getArea()+"; &&& Chu vi: "+square2.getPerimeter());
        System.out.println("square1: Diện tích : "+square3.getArea()+"; &&& Chu vi: "+square3.getPerimeter());
    
    }
}

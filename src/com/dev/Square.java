package com.dev;
import com.dev.Shape;
public class Square extends Rectangle {
   
   private double side;
   
    public Square() {
    }

    public Square(double side) {
        super(side,side);
        this.side=side;
    }

    public Square(String color, boolean filled, double side) {
        super(color, filled, side, side);
        this.side=side;
    }
    public double getSide(){
        return side;
    }
    public void setSide(){
        this.side=side;
        setWidth(side);
        setLength(side);
    }
    @Override
    public double getArea() {
        // TODO Auto-generated method stub
        return super.getArea();
    }
    
    @Override
    public double getLength() {
        // TODO Auto-generated method stub
        return super.getLength();
    }

    @Override
    public double getPerimeter() {
        // TODO Auto-generated method stub
        return super.getPerimeter();
    }

    @Override
    public double getWidth() {
        // TODO Auto-generated method stub
        return super.getWidth();
    }

    @Override
    public void setLength(double length) {
        // TODO Auto-generated method stub
        super.setLength(side);
    }

    @Override
    public void setWidth(double width) {
        // TODO Auto-generated method stub
        super.setWidth(side);
    }

    @Override
    public String getColor() {
        // TODO Auto-generated method stub
        return super.getColor();
    }

    @Override
    public boolean isFilled() {
        // TODO Auto-generated method stub
        return super.isFilled();
    }

    @Override
    public void setColor(String color) {
        // TODO Auto-generated method stub
        super.setColor(color);
    }

    @Override
    public void setFilled(boolean filled) {
        // TODO Auto-generated method stub
        super.setFilled(filled);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        // TODO Auto-generated method stub
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        // TODO Auto-generated method stub
        return super.equals(obj);
    }

    @Override
    protected void finalize() throws Throwable {
        // TODO Auto-generated method stub
        super.finalize();
    }

    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString() + ", side: " + side;
    }
   
}
